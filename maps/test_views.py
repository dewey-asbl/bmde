from django.test import TestCase
from .views import get_and_check_csv_data
import csv


class CSVImportTestCase(TestCase):
    required_fields = ['name', 'lat', 'lon', 'address']
    optional_fields = ['comment', 'web', 'email', 'phone']

    def get_header_line(self, data):
        r = list(csv.reader(data.split('\n'), delimiter=','))
        return list(csv.reader(data.split()))[0]

    def get_other_lines(self, data):
        return list(csv.reader(data.split('\n'), delimiter=','))[1:]

    def test_get_and_check_csv_data_error_lon_lat(self):
        """ Check if the detection of unvalide lon/lat works"""
        data = """name,lat,lon,address
hamac de rio,4.405518,51.212045,a rio
lune,4.942474,300,dans les etoiles"""

        ret = get_and_check_csv_data(
            self.required_fields, self.optional_fields,
            self.get_header_line(data), self.get_other_lines(data))

        self.assertTrue(ret[0] is None)
        self.assertEquals(ret[1], 'It is not possible to create the point lune \
with lat: 4.942474 and lon: 300.0')

    def test_get_and_check_csv_data_error_empty_field(self):
        """ Check if the detection empty required field works"""
        data = """name,lat,lon,address
Chez margueritte,4.405518,51.212045,22 rue des paquerettes
Chez mbiau mbell,4.423542,51.215594,ancienne gare de cuesmes
,4.942474,51.323747,qui est-ce ?
Chez hector,4.942474,50.911692,dans le sac de Marc"""

        ret = get_and_check_csv_data(
            self.required_fields, self.optional_fields,
            self.get_header_line(data), self.get_other_lines(data))

        self.assertTrue(ret[0] is None)
        self.assertEquals(ret[1], 'One line has its field "name" empty')

    def test_get_and_check_csv_data_error_missing_column(self):
        """ Check if the detection of a missing required column works"""
        data = """name,lat,address
Chez margueritte,4.405518,22 rue des paquerettes
Chez mbiau mbell,4.423542,ancienne gare de cuesmes
Chez hector,50.911692,dans le sac de Marc"""

        ret = get_and_check_csv_data(
            self.required_fields, self.optional_fields,
            self.get_header_line(data), self.get_other_lines(data))

        self.assertTrue(ret[0] is None)
        self.assertEquals(ret[1], 'lon not in the header.')

    def test_get_and_check_csv_data_works_with_empty_line(self):
        """ Check the function get_and_check_csv_data works with empty lines"""
        data = """lat,name,lon,address
4.405518,Chez margueritte,51.212045,22 rue des paquerettes
4.423542,Chez mbiau mbell,51.215594,ancienne gare de cuesmes
,,,
4.942474,Chez hector,50.911692,dans le sac de Marc
,,,"""

        ret = get_and_check_csv_data(
            self.required_fields, self.optional_fields,
            self.get_header_line(data), self.get_other_lines(data))

        self.assertTrue(len(ret[0]) == 3)
        self.assertEquals(ret[0][0], {
            'lat': 4.405518, 'name': 'Chez margueritte', 'lon': 51.212045,
            'address': '22 rue des paquerettes'})
        self.assertEquals(ret[0][1], {
            'address': 'ancienne gare de cuesmes', 'name': 'Chez mbiau mbell',
            'lat': 4.423542, 'lon': 51.215594})
        self.assertEquals(ret[0][2], {
            'address': 'dans le sac de Marc', 'name': 'Chez hector',
            'lat': 4.942474, 'lon': 50.911692})
