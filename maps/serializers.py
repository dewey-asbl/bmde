from maps.models import Marker, ResourceManagerType
from rest_framework import serializers, generics
# from closet.serializers import SubcategorySerializer


class ResourceManagerTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResourceManagerType
        fields = ('code', 'name')


class MarkerSerializer(serializers.HyperlinkedModelSerializer):
    subcategories = serializers.SerializerMethodField()
    manager_type = ResourceManagerTypeSerializer(read_only=True)
    history = serializers.SerializerMethodField()

    def get_subcategories(self, obj):
        if obj:
            return [
                {'id': x.id, 'name': x.name} for x in obj.subcategories.all()]

    def get_history(self, obj):
        return self.context.get('request') \
            .build_absolute_uri('/api/markers/{}/history'.format(obj.id))

    class Meta:
        model = Marker
        fields = (
            'name', 'lat', 'lon', 'subcategories', 'popup', 'comment', 'web',
            'phone', 'address', 'manager_type', 'created', 'id',
            'last_modified', 'last_author', 'history', 'email_before_at',
            'email_after_at', 'last_modified_dmY')


class MakersBySubCategoriesList(generics.ListAPIView):
    """ A view that display all the markers for a given subcategories """
    serializer_class = MarkerSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        subcategory_id = self.kwargs['subcategory_id']
        return Marker.objects.filter(subcategories__id=subcategory_id)
