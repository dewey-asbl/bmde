from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from maps.models import Marker, ResourceManagerType
from reversion.admin import VersionAdmin


class ResourceManagerTypeAdmin(admin.ModelAdmin):
    pass


class MarkerAdmin(LeafletGeoAdmin, VersionAdmin):
    list_display = ('name', 'web', 'phone', 'address', 'public', 'last_modified')
    list_filter = ('public', 'subcategories', 'created')
    search_fields = ('name', 'comment', 'web')
    readonly_fields = ('last_modified',)

admin.site.register(Marker, MarkerAdmin)
admin.site.register(ResourceManagerType, ResourceManagerTypeAdmin)
