module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bmde: {
      folders: {
        fonts: './fonts',
        bower: './bower_components/',
        css: './css/',
        js: './js/',
        sass: './sass/',
      }
    },
    bower: {
      install: {
        options: {
           verbose: true
        }
      }
    },
    copy: {
        snap: {
           files: [
               {
                   cwd: '<%= bmde.folders.bower %>Snap.js',
                   src: ['snap.css'],
                   dest: 'lib/Snap.js',
                   expand: true,
               }
           ]
        },
        qtip2: {
           files: [
               {
                   cwd: '<%= bmde.folders.bower %>qtip2',
                   src: ['jquery.qtip.min.css'],
                   dest: 'lib/qtip2',
                   expand: true,
               }
           ]
        },
        font_awesome: {
           files: [
               {
                   cwd: '<%= bmde.folders.bower %>font-awesome',
                   src: ['css/font-awesome.min.css', 'fonts/*'],
                   dest: 'lib/font-awesome',
                   expand: true,
               }
           ]
        },
        foundation: {
            files: [
                {
                    cwd: '<%= bmde.folders.bower %>foundation',
                    src: ['css/normalize.css', 'js/vendor/modernizr.js'],
                    dest: 'lib/foundation',
                    expand: true,
                }
            ]
        },
        leaflet: {
            files: [
                {
                    cwd: '<%= bmde.folders.bower %>leaflet/dist/images/',
                    src: ['marker-icon.png', 'marker-shadow.png'],
                    dest: 'lib/leaflet/images',
                    expand: true,
                }
            ]
        }
    }
  });

  grunt.loadNpmTasks('grunt-bower-task');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Default task(s).
  grunt.registerTask('default', ['bower', 'copy']);

};
