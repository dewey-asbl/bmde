/* global define */
'use strict';

define(['jquery', 'markers', 'cluster', 'map', 'leaflet'],
    function($, markers, cluster, map, L) {
        var shown_subcat_id = []; // the list of the subcat selected
        var subcats_feat_subgroup_buffer = {};

        function watchSubCatSelectionMenu() {
            /**
             * Watch which subcategories have been selected to be displayed :
             * - if a subcategory is selected, all its markers will be displayed on the map
             * - if a subcategory is unselected, all its markers will be removed from the map
             */
            $('.subcat').each(function(){
                $(this)
                    .attr('show', "")
                    .click(function(){
                        var id = $(this).data("id");
                        if(shown_subcat_id.indexOf(id) != -1) { // The category is displayed
                            // We remove if from the array shown_subcat_id
                            shown_subcat_id.splice(shown_subcat_id.indexOf(id),1);
                            hideMakersFor(id);
                            $(this).attr('show', "");
                        } else { // The category is not displayed -> to display
                            shown_subcat_id.push(id);
                            displayMakersFor(id);
                            $(this).attr('show', "ok");
                        }
                    });
            });
        }

        function getDataMarkersFor(subcat_id, callback) {
            /**
             * Get an array of the data of all the markers of a subcategory
             * @param {integer} subcat_id The id of the subcategory
             * @param {function} callback Callback to execute on the markers
             * description
             */

            $.ajax({
              url: "/api/subcategories/" + subcat_id + "/markers"
            }).done(function(response) {
                callback(response);
            });
        }

        function getFeatureSubGroupFor(subcat_id, callback) {
            /**
             * Get the FeatureSubGroup containing all the markers of a
             * subcategory
             * @param {integer} subcat_id The id of the subcategory
             * @param {function taking a featureSubGroup as agument} callback
             * The callback to execute on the featureSubGroup
             */
            if(subcat_id in subcats_feat_subgroup_buffer) {
                callback(subcats_feat_subgroup_buffer[subcat_id]);
            } else {
                getDataMarkersFor(subcat_id, function(subcats_data) {
                    var subcat_featureGroup =  L.featureGroup.subGroup();
                    subcats_data.forEach(function(point_data) {
                        point_data.color = $("#subcat-"+subcat_id).data("color");
                        var marker = markers.getMarker(point_data);
                        marker.addTo(subcat_featureGroup);
                    });
                    subcats_feat_subgroup_buffer[subcat_id] = subcat_featureGroup;
                    callback(subcat_featureGroup);
                });
            }
        }

        function displayMakersFor(subcat_id) {
            /**
             * Display on the map the markers for a given subcategory
             * @param {integer} subcat_id The id of the subcategory
             */
            getFeatureSubGroupFor(subcat_id, function(markerGroup) {
                cluster.addFeatureGroup(markerGroup);
            });
        }

        function hideMakersFor(subcat_id) {
            /**
             * Hide from the map the markers for a given subcategory
             * @param {integer} subcat_id The id of the subcategory
             */
            getFeatureSubGroupFor(subcat_id, function(markerGroup) {
                cluster.rmFeatureGroup(markerGroup);
            });
        }

        return {
            watchSubCatSelectionMenu: watchSubCatSelectionMenu,
        };
    }
);
